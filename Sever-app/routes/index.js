    var express = require('express');
    var router = express.Router();
    const sinhViens = require("../model/SinhVien");
    const admin=require("../model/Admin");
    const services = require('../services/render');
    const controller = require('../controller/index.controller');

    router.get('/', function(req, res) {
        res.render('login');
    });
    router.post('/submit', async function(req, res) {

        var emailAdmin= req.body.emailAdmin;
        var passwordAdmin= req.body.passwordAdmin;
            const cursor = await admin.findOne({ email: emailAdmin }).exec();
            try {
                if(cursor != null){
                    if(cursor.password === passwordAdmin){
                        console.log("login thành công sv");
                        res.redirect('/home');
                    }else{
                    }
                }else{
                    console.log('data null');
                }
            }catch(err){
                console.log('data null');
            }

    });
    router.get('/product', function(req, res) {
        res.render('product');
    });

    router.post('/product/submit', function(req, res) {
        var nameInput= req.body.nameInput;
        var mssvInput= req.body.mssvInput;
        var emailInput= req.body.emailInput;
        var nganhInput= req.body.nganhInput;
        var gioiTinhInput= req.body.gioiTinhInput;
        var sinhVien = { mssv: mssvInput, email: emailInput, ten: nameInput,nganh:nganhInput,gioiTinh :gioiTinhInput };
        sinhViens.create(sinhVien, function (err, result) {
            if (err) {
                console.log(err);
            } else {
                console.log('Inserted new sinh viên');

            }
        });
        res.redirect('/listProduct');
    });
    router.get('/listProduct', services.homeRoutes);
    router.get('/updateProduct', services.update_user);
    router.get('/deleteProduct', services.delete);
    router.post('/updateProduct/submit',function(req, res) {
        var nameInput= req.body.nameInput;
        var mssvInput= req.body.mssvInput;
        var emailInput= req.body.emailInput;
        var nganhInput= req.body.nganhInput;
        var gioiTinhInput= req.body.gioiTinhInput;
        sinhViens.findByIdAndUpdate(req.body.idInput, { $set: { mssv: mssvInput, email: emailInput, ten: nameInput, nganh : nganhInput, gioiTinh :gioiTinhInput} },
            function(err, result) {
               if (err) {
                    console.log(err);
               } else {    
                    res.redirect('/listProduct');          
               }
           });  
    });
    router.get('/home',services.chart);

    module.exports = router;