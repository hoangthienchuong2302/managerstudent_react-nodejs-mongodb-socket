const SinhVien = require('../model/SinhVien');


module.exports.index = (req, res) => {
    // res.send('index')

    SinhVien.find({}, (err, sinhViens) => {
        res.json(sinhViens);
    });
}

module.exports.insert = (req, res) => {
    res.render('form')
}

module.exports.insertPost = (req, res) => {

    SinhVien.create({
        mssv: req.body.mssv,
        email: req.body.email,
        ten: req.body.ten,
        tuoi: req.body.tuoi,
    }, (err, small) => {
        if (!err) {
            res.redirect('/')
        } else {

            res.send(err);
        }
    });
}