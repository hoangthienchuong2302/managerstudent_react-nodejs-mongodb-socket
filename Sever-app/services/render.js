const sinhVien=require("../model/SinhVien");

exports.homeRoutes = (req, res) => {
    sinhVien.find({}).then(response => {
        res.render('listProduct',{sinhViens : response});

      })
      .catch(err =>{
        res.send(err);
    })
  
}

exports.chart = (req, res) => {

  sinhVien.find({}).then(response => {
    var cntt = 0;
    var qtks = 0;;
    var mkt = 0;
    var hdvdl = 0;
    var tcsk = 0;
    for (var i = 0; i < response.length; i++) {
        if (response[i].nganh === "Công nghệ thông tin") {
            cntt++;
        } else if (response[i].nganh === "Quản trị khách sạn") {
            qtks++;
        } else if (response[i].nganh === "Marketting") {
            mkt++;
        } else if (response[i].nganh === "Hướng dẫn viên du lịch") {
            hdvdl++;
        } else if (response[i].nganh === "Tổ chức sự kiện") {
            tcsk++;
        }
        console.log(cntt);
    }
      res.render('home',{cntt : cntt, qtks : qtks, mkt : mkt, hdvdl : hdvdl, tcsk : tcsk});
    })
    .catch(err =>{
      res.send(err);
  })



}

exports.update_user = (req, res) =>{
  sinhVien.findOne({_id : req.query.id}).then(response => {
    res.render('updateProduct',{sinhViens : response});
  })
  .catch(err =>{
    res.send(err);
})
   
}
exports.delete = (req, res)=>{

  sinhVien.findByIdAndDelete( req.query.id)
      .then(data => {
          if(!data){
              res.status(404).send({ message : `Cannot Delete with id ${req.query.id}. Maybe id is wrong`})
          }else{
            res.redirect('/listProduct');    
          } 
      })
      .catch(err =>{
          res.status(500).send({
              message: "Could not delete User with id=" + req.query.id
          });
      });
}
