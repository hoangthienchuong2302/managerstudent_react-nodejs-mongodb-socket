var mongoose = require('mongoose');

var sachSchema = new mongoose.Schema({
    MaSach: String,
    TenSach: String,
    TheLoai: String,
    TacGia: String,
    NXB: String,
});

var Sach = mongoose.model('QuanLys', sachSchema);

module.exports = Sach;