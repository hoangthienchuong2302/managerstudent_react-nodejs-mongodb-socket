var mongoose = require('mongoose');

var sinhVienSchema = new mongoose.Schema({
    mssv: String,
    email: String,
    ten: String,
    gioiTinh: String,
    nganh: String,
});

var SinhVien = mongoose.model('SinhVien', sinhVienSchema);

module.exports = SinhVien;