const mongoose = require("mongoose");

async function connect() {
    try {
        await mongoose.connect("mongodb://localhost:27017/QLSV", {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        console.log("Connect successfully!");
    } catch (error) {
        console.log("Error:" + error);
    }
}

module.exports = { connect };